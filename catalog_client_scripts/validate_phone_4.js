//source: https://community.servicenow.com/message/984376#984376
function onLoad() {
    var phno_ctrl = g_form.getControl('validate_phone_4');
    phno_ctrl.onblur = appenddash;

}

function appenddash() {
    var newPhone;
    var phno = this.value;
    var phoneval1 = /^\d{14}$/;
    var phoneval2 = /^\d{10}$/;
    g_form.hideFieldMsg(this, true);
    if ((phno !== "") && !phno.match(phoneval1) && phno.length > 10) {
        g_form.showFieldMsg(this, 'The phone number should be in a numbers with 14 digits if extn is available', 'error');
        this.value = '';
    } else {
        newPhone = '(' + phno.substr(0, 3) + ')' + phno.substr(3, 3) + '-' + phno.substr(6, 4) + ' extn' + phno.substr(10, 4);
    }

    if ((phno !== "") && !phno.match(phoneval1) && phno.length < 10) {
        g_form.showFieldMsg(this, 'The phone number should be in a numbers with 10 digits if extn is not available', 'error');
        this.value = '';
    } else {
        newPhone = '(' + phno.substr(0, 3) + ')' + phno.substr(3, 3) + '-' + phno.substr(6, 4);
    }
    this.value = newPhone;
}