//source: https://community.servicenow.com/message/914311#914311
function onChange(control, oldValue, newValue, isLoading) {
    if (isLoading || newValue === '') {
        return;
    }

    var newNum = '';
    var userPhone = newValue;
    userPhone = userPhone.replace(/[^\d]/g, '');

    //g_form.showFieldMsg('u_telephone_number',"phone number = " + userPhone,'error');
    var testDigitsPattern = /^[0-9]{10}$/;

    var testResult = testDigitsPattern.test(userPhone);
    //g_form.showFieldMsg('u_telephone_number',"Result = " + testResult,"error");

    if (!testDigitsPattern.test(userPhone)) {
        control.value = '';
        g_form.showFieldMsg(control, "Invalid telephone number.  Please enter your telephone number including area code.", 'error');
    }

    if (testDigitsPattern.test(userPhone)) {
        g_form.hideFieldMsg(control, true);
        newNum = '(' + userPhone.substring(0, 3) + ') ' + userPhone.substring(3, 6) + ' - ' + userPhone.substring(6, 10);
        control.value = newNum;
    }
}