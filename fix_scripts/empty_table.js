emptyTable('');

function emptyTable(table) {
    var gr = new GlideRecord(table);
    gr.query();
    gr.deleteMultiple();
}