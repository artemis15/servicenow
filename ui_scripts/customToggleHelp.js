//generally you'll just call
//customToggleHelp('varname' or 'field_name');
/*global document, $j*/
function customToggleHelp(varname) {
    try {
        if (g_form.getTableName() == "ni") {
            customHelp(varname, 'slide');
        } else {
            customTaskHelp(varname);
        }
    } catch (e) {
        console.error('Error toggling Help - varname:' + varname);
    }
}

function customHelp(varname, action) {
    var myVar1 = g_form.getControl(varname);
    myVar1 = 'question_help_IO_' + myVar1.id.split(':')[1].split('_')[0] + '_toggle';
    var element = document.getElementById(myVar1);
    _toggleIcon(element, "icon-vcr-down", _getVcrIconClass());
    if (action.toLowerCase() == 'slide') {
        $j("#" + element.id + "_value").slideToggle();
    }
    if (action.toLowerCase() == 'slidedown') {
        $j("#" + element.id + "_value").slideDown();
    }
    if (action.toLowerCase() == 'slideup') {
        $j("#" + element.id + "_value").slideUp();
    }
    _frameChanged();
}

function customTaskHelp(varname) {
    g_form.nameMap.map(function(i) {
        if (i.prettyName == varname) {
            var realID = i.realName;
            var a = $j('#question_help_ni_VE' + realID + '_toggle')[0];
            toggleVariableHelpText(a);
            return;
        }
    });
}
